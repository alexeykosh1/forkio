"Step project - Forkio" is a learning project of students Dan IT Education.

Two students participated in developing the project:
Sergey Primakov (student_1)
Alexey Koshelivskiy(student_2).

During developing the following technologies have been used:

1) HTML with BEM classname approach.
2) SCSS with contemporary technologies such as Flexbox, Grid, etc.
3) Scripts written on javascripts basis.
4) Gulp has been used for building the project.

Student_1 developed Header section and section "People Are Talking About Fork".
Student_2 developed sections: "Revolutionary editor", "Here is what you get" and "Fork Subscription Pricing".

Both students participated in developing setup and building the project.

